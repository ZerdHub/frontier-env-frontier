import React from "react";

import "./../styles/index.scss";

import {AgencySection, HeaderSection, HeroSection} from "../sections";

export default () => (
  <section id="HomePage">

    <HeroSection/>

    <HeaderSection/>

    <AgencySection/>

  </section>
);
