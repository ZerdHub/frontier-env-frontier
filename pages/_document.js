import Document, {Head, Main, NextScript} from 'next/document'

export default class MyDocument extends Document {

  render() {

    return (
      <html>

      <Head>
        <meta charSet="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <link rel="stylesheet" href={"/_next/static/style.css"}/>
        <link rel="stylesheet" href={"https://fonts.googleapis.com/css?family=Gothic+A1:100,300,400,700,900"}/>
        <title>Zerd Studio</title>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/solid.css"
              integrity="sha384-Rw5qeepMFvJVEZdSo1nDQD5B6wX0m7c5Z/pLNvjkB14W6Yki1hKbSEQaX9ffUbWe"
              crossorigin="anonymous"/>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/fontawesome.css"
              integrity="sha384-GVa9GOgVQgOk+TNYXu7S/InPTfSDTtBalSgkgqQ7sCik56N9ztlkoTr2f/T44oKV"
              crossorigin="anonymous"/>
      </Head>

      <body>
      <Main/>
      <NextScript/>
      </body>

      </html>
    )

  }

}
