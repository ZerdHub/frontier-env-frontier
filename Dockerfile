# specify the node base image with your desired version node:<version>

FROM node:alpine

# replace this with your application's default port

EXPOSE 3000

# Set the workspace to /opt

WORKDIR /opt

# Transfer the workspace /opt

COPY index.js /opt
COPY package.json /opt
COPY package-lock.json /opt
COPY next.config.js /opt

COPY blocks /opt/blocks
COPY pages /opt/pages
COPY sections /opt/sections
COPY static /opt/static
COPY styles /opt/styles

# Bootstrap

RUN npm install && npm run build

CMD npm start
