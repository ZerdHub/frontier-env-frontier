import PropTypes from "prop-types";
import React from "react";

class FeaturedBlock extends React.Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {

    return (
      <section className="FeaturedBlock">

        <div className="icon">
          <i className={["fas", "fa-fw", this.props.icon].join(" ")} />
        </div>

        <p className="title">
          {this.props.title}
        </p>

        <p className="content">
          {this.props.children}
        </p>

      </section>
    );

  }

}

FeaturedBlock.propTypes = {
  icon: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node)
  ])
};

export default FeaturedBlock;
