const express = require("express");
const next = require("next");

const app = next({
  dev: process.env.NODE_ENV !== "production"
});

const handle = app.getRequestHandler();

app.prepare()
  .then(function () {

    const server = express();

    server.use(function (req, res, next) {

      if (process.env.NODE_ENV === "production" && (req.path.startsWith("/static") || req.path.startsWith("/_next")))

        res.set("Expires", "8h");

      next();

    });

    server.get("*", function (req, res) {
      return handle(req, res);
    });

    server.listen(process.env.PORT || 3000, function (error) {

      if (error)

        throw error;

      console.log("> Ready on port", process.env.PORT || 3000);

    });

  })
  .catch(function (error) {
    throw error;
  });
