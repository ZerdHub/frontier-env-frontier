import Link from "next/link";
import React from "react";

import {FeaturedBlock} from "./../../blocks";

class AgencySection extends React.Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {

    return (
      <section id="AgencySection">

        <div className="container section-heading">

          <div className="title">
            A digital <span className="text-primary">web design</span> studio creating
            modern & engaging online experiences
          </div>

          <div className="sub-title">
            It is a long established fact that a reader will be distracted
            by the readable content of a page when looking at its layout.
          </div>

          <div className="action">
            <Link href="/about">
              <button className="btn btn-outline-secondary">
                <i className="fas fa-play mr-3" />
                Watch Our Film
              </button>
            </Link>
          </div>

        </div>

        <div className="container section-featured">

          <div className="row">

            <div className="col-md-4">

              <FeaturedBlock icon="fa-magic" title="Modern Webflow">
                It is a long established fact that a reader will be distracted
                by the readable content of a page when looking at its layout.
              </FeaturedBlock>

            </div>

            <div className="col-md-4">

              <FeaturedBlock icon="fa-paint-brush" title="Beautifully Designed">
                It is a long established fact that a reader will be distracted
                by the readable content of a page when looking at its layout.
              </FeaturedBlock>

            </div>

            <div className="col-md-4">

              <FeaturedBlock icon="fa-mobile-alt" title="Mobile Friendly">
                It is a long established fact that a reader will be distracted
                by the readable content of a page when looking at its layout.
              </FeaturedBlock>

            </div>

          </div>

        </div>

      </section>
    );

  }

}

export default AgencySection;
