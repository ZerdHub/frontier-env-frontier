import Link from "next/link";
import React from "react";

class HeaderSection extends React.Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {

    return (
      <section id="HeaderSection">

        <div className="container-fluid">

          <div className="row">

            <div className="col-md-3 section-brand">
              <Link href="/">
                <a className="brand">Zerd.</a>
              </Link>
            </div>

            <div className="col-md-9 section-navigation">

              <Link href="/">
                <a className="record">Home</a>
              </Link>

              <Link href="#agency">
                <a className="record">Agency</a>
              </Link>

              <Link href="#service">
                <a className="record">Service</a>
              </Link>

              <Link href="#pricing">
                <a className="record">Pricing</a>
              </Link>

              <Link href="#portfolio">
                <a className="record">Portfolio</a>
              </Link>

              <Link href="#contact">
                <a className="record">Contact</a>
              </Link>

            </div>

          </div>

        </div>

      </section>
    );

  }

}

export default HeaderSection;
