import Link from "next/link";
import React from "react";

class HeroSection extends React.Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {

    return (
      <section id="HeroComponent">

        <div className="section-gradient"/>

        <div className="section-inner">

          <div className="heading">
            Zerd Studio
          </div>

          <div className="sub-heading">
            Build your website in moment
          </div>

          <div className="action">
            <Link href={"/portfolio"}>
              <button className="btn btn-outline-secondary">
                See Our Portfolio
              </button>
            </Link>
          </div>

        </div>

        <div className="section-divider" dangerouslySetInnerHTML={{__html: generateDividerInnerHTML()}}/>

      </section>
    );

  }

}

export default HeroSection;

/**
 * @returns {string}
 */
function generateDividerInnerHTML() {
  return `<svg class="section-divider" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 140" style="margin-bottom: -9px;enable-background:new 0 0 1920 140;" xml:space="preserve"><path class="svg-white" d="M960,92.9C811.4,93.3,662.8,89.4,515.3,79c-138.6-9.8-277.1-26.2-409-53.3C97.8,24,0,6.5,0,0c0,0,0,140,0,140 l960-1.2l960,1.2c0,0,0-140,0-140c0,2.7-42.1,11.3-45.8,12.2c-45.1,11-91.5,20.1-138.4,28.1c-176.2,30.1-359.9,43.8-542.9,48.9 C1115.4,91.4,1037.7,92.7,960,92.9z"></path></svg>`;
}
